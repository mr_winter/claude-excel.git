![React_Chat](./asset/screely-1690438962227.png)
# Claude_Excel
一个基于基于Claude GPT快速完成Excel工资自动核算模板，使用简单

> Note 
> 本项目来源于[《腾讯云 Cloud Studio 实战训练营》](https://marketing.csdn.net/p/06a21ca7f4a1843512fa8f8c40a16635)的参赛作品，该作品在腾讯云 [Cloud Studio](https://www.cloudstudio.net/?utm=csdn) 中运行无误。


# 使用方法

## 一、使用git
### 1. 克隆代码
```bash
git clone https://gitee.com/mr_winter/claude-excel.git
```
### 2. 安装依赖
```bash
pip install pandas
pip install openpyxl
```
### 3. 运行
```bash
python demo.py
```
## 二、使用Cloud Studio
### 1. 找到模板并创建
点击按钮直达模板地址

[![Cloud Studio Template](https://cs-res.codehub.cn/common/assets/icon-badge.svg)](https://cloudstudio.net/templates/rJJUD2vCdxe)

### 2. 安装依赖
```bash
pip install pandas
pip install openpyxl
```

### 3. 运行
```bash
python demo.py
```