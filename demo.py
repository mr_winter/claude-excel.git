import pandas as pd
df = pd.read_excel('salary.xlsx')
# 考勤扣除金额计算
df['考勤扣除金额'] = (df['迟到次数'] - 3).clip(lower=0) * 100
# 个税扣除金额计算
df['个税扣除'] = 0
taxable_income = df['工资基数'] - df['五险一金扣除'] - df['考勤扣除金额']
df.loc[taxable_income <= 3000, '个税扣除'] = taxable_income * 0.03
df.loc[(taxable_income > 3000) & (taxable_income <= 12000), '个税扣除'] = taxable_income * 0.1
df.loc[(taxable_income > 12000) & (taxable_income <= 25000), '个税扣除'] = taxable_income * 0.2
df.loc[(taxable_income > 25000) & (taxable_income <= 35000), '个税扣除'] = taxable_income * 0.25
df.loc[(taxable_income > 35000) & (taxable_income <= 55000), '个税扣除'] = taxable_income * 0.3
df.loc[(taxable_income > 55000) & (taxable_income <= 80000), '个税扣除'] = taxable_income * 0.35
df.loc[taxable_income > 80000, '个税扣除'] = taxable_income * 0.45

# 实发工资计算 
df['实发工资'] = df['工资基数'] - df['五险一金扣除'] - df['考勤扣除金额'] - df['个税扣除']

print(df)

# 将计算结果写入Excel
df.to_excel('salary_output.xlsx', index=False)
print('计算结果已写入Excel表格salary_output.xlsx') 